import numpy as np
import matplotlib.pyplot as plt


def get_points(fig):
    # https://stackoverflow.com/a/48174228
    return (fig.gca().get_window_extent().width / 10 * 72. / fig.dpi) ** 2


# modified from https://github.com/dykuang/Medical-image-registration/blob/master/source/visual.py
def vis_grid_displacements(displacements, subsample=1, directions=2, fig=None, color='orchid', xv=None, yv=None, **kwargs):
    """
    Visualize deformation of grid.
    :param displacements: displacement field of shape (num_x, num_y, 2)
    :param subsample: subsample using only every "subsample" value of the input
    :param directions: which direction to show. 0: x, 1:y, 2: both
    :param fig: figure into which the data is plotted
    :param color: color
    :param xv: x coordinates in 'ij' meshgrid form, default from -1 to 1
    :param yv: y coordinates in 'ij' meshgrid form, default from -1 to 1
    :return: figure
    """

    # xy is of shape h*w*2
    w, h = np.shape(displacements)[0], np.shape(displacements)[1]

    if xv is None or yv is None:
        x = np.linspace(-1., 1., w)
        y = np.linspace(-1., 1., h)
        xv, yv = np.meshgrid(x, y)

    xy = np.stack([xv, yv], 2) + displacements

    return vis_grid(xy, subsample=subsample, directions=directions, fig=fig, color=color, xv=xv, yv=yv, **kwargs)


# modified from https://github.com/dykuang/Medical-image-registration/blob/master/source/visual.py
def vis_grid(values, subsample=1, directions=2, fig=None, ax=None, color='orchid', xv=None, yv=None, **kwargs):
    """
    Visualize deformation of grid.
    :param values: new coordinate values of grid of shape (num_x, num_y, 2)
    :param subsample: subsample using only every "subsample" value of the input
    :param directions: which direction to show. 0: x, 1:y, 2: both
    :param fig: figure into which the data is plotted
    :param color: color
    :param xv: x coordinates in 'ij' meshgrid form, needed if directions == 1
    :param yv: y coordinates in 'ij' meshgrid form, needed if directions == 0
    :return: figure
    """

    # xy is of shape h*w*2
    w, h = np.shape(values)[0], np.shape(values)[1]

    xy = values

    xx_displaced = xy[..., 0]
    yy_displaced = xy[..., 1]

    if ax is None:
        if fig is None:
            fig = plt.figure()
        ax = fig.gca()
        # ax = fig.add_axes([0.1, 0.01, 1, 1])

    if directions == 0 and yv is not None:  # Only plot the x-direction
        for row in range(0, w, subsample):
            x, y = xx_displaced[row, :], yv[row, :]
            ax.plot(x, y, color=color, **kwargs)
        for col in range(0, h, subsample):
            x, y = xx_displaced[:, col], yv[:, col]
            ax.plot(x, y, color=color, **kwargs)
    elif directions == 1 and xv is not None:  # Only plot the y-direction
        for row in range(0, w, subsample):
            x, y = xv[row, :], yy_displaced[row, :]
            ax.plot(x, y, color=color, **kwargs)
        for col in range(0, h, subsample):
            x, y = xv[:, col], yy_displaced[:, col]
            ax.plot(x, y, color=color, **kwargs)
    else:
        for row in range(0, w, subsample):
            x, y = xx_displaced[row, :], yy_displaced[row, :]
            ax.plot(x, y, color=color, **kwargs)
        if subsample > 1:
            x, y = xx_displaced[-1, :], yy_displaced[-1, :]
            ax.plot(x, y, color=color, **kwargs)
        for col in range(0, h, subsample):
            x, y = xx_displaced[:, col], yy_displaced[:, col]
            ax.plot(x, y, color=color, **kwargs)
        if subsample > 1:
            x, y = xx_displaced[:, -1], yy_displaced[:, -1]
            ax.plot(x, y, color=color, **kwargs)
    # ax.set_ylim(-1, 1)
    # ax.set_axis_off()
    ax.set_aspect('equal')
    return fig


def vis_arrows(displacements, subsample=1, fig=None, color='orchid', xv=None, yv=None):
    """
    Visualize deformation of grid.
    :param displacements: displacement vectors of shape (num_x, num_y, 2)
    :param subsample: subsample using only every "subsample" value of the input
    :param fig: figure into which the data is plotted
    :param color: color
    :param xv: x coordinates in 'ij' meshgrid form, needed if directions == 1
    :param yv: y coordinates in 'ij' meshgrid form, needed if directions == 0
    :return: figure
    """

    # xy is of shape h*w*2
    w, h = np.shape(displacements)[0], np.shape(displacements)[1]

    if xv is None or yv is None:
        x = np.linspace(-1., 1., w)
        y = np.linspace(-1., 1., h)
        xv, yv = np.meshgrid(x, y)

    dxv = displacements[..., 0]
    dyv = displacements[..., 1]

    if fig is None:
        fig = plt.figure()
        # ax = fig.add_axes([0.1, 0.01, 1, 1])
    ax = fig.gca()
    ax.set_ylim(-1, 1)
    ax.set_xlim(-1, 1)

    for row in range(0, w, subsample):
        for x, y, dx, dy in zip(xv[row, :], yv[row, :], dxv[row, :], dyv[row, :]):
            ax.arrow(x, y, dx, dy, color=color, head_width=0.05, head_length=0.03, length_includes_head=True,
                     fill=False, overhang=0.8)
    # ax.set_axis_off()
    ax.set_aspect('equal')
    return fig
