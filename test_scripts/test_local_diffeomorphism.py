import torch
from plotting_utils import vis_grid_displacements, vis_arrows
import numpy as np
from local_diffeomorphism_networks import SumOfVelocityFields

config_dict = [{'type': 'scaling'}, {'type': 'rotation'}, {'type': 'translation'}] * 2
x = np.linspace(-1, 1, 10)
xv, yv = np.meshgrid(x, x, indexing='ij')
xy = np.stack([xv, yv], axis=2)
xy_flat = xy.reshape(-1, 2)

test_cls = SumOfVelocityFields(config_dict, scale=1)

out = test_cls(torch.tensor(xy_flat, dtype=torch.float)).detach().numpy()

fig = vis_grid_displacements(out.reshape(xy.shape), xv=xv, yv=yv)
fig.show()
# fig = vis_arrows(out.reshape(xy.shape), fig=fig, xv=xv, yv=yv)
# fig.show()

bla = test_cls.local_velocity_fields
print(bla)
