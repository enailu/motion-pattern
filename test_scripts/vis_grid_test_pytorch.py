from plotting_utils import vis_grid_displacements, vis_arrows, vis_grid
import numpy as np
import torch
from velocity_fields_pytorch import LocalScaling, LocalTranslation, LocalRotation, LocalAnisotropicScaling

x = np.linspace(-1, 1, 10)
xv, yv = np.meshgrid(x, x, indexing='xy')
xy = np.stack([xv, yv], axis=2)
num_x, num_y = xv.shape

displacements = np.zeros_like(xy)
# displacement_x = np.exp(-(xv)**2)*np.exp(-(x+1)**2/4)
# displacement_y = np.zeros_like(yv)
# displacements = np.stack([displacement_x, displacement_y], axis=2)

v_translation = LocalTranslation(initial_direction=[(1, 1)], initial_center=[(-0.5, -0.5)],
                                    scale=0.5)
fig = vis_grid_displacements(displacements, subsample=2)
# fig1 = vis_grid_displacements(v_translation(torch.tensor(xy.reshape(-1, 2))).detach().numpy().reshape(num_x, num_y, -1), color='blue')
# fig1.show()
# fig2 = vis_arrows(v_translation(torch.tensor(xy.reshape(-1, 2))).detach().numpy().reshape(num_x, num_y, -1), fig=None, xv=xv, yv=yv, subsample=1)
# fig2.show()

v_scaling = LocalScaling(initial_center=[(0, 0)], scale=0.5)
# displacements = v_scaling(torch.tensor(xy.reshape(-1, 2))).detach().numpy().reshape(num_x, num_y, -1)
# fig1 = vis_grid_displacements(-displacements, xv=xv, yv=yv)
# fig1 = vis_grid_displacements(displacements, fig=fig1, xv=xv, yv=yv, color='blue')
# fig1.show()
# fig2 = vis_arrows(displacements, xv=xv, yv=yv)
# fig2.show()

v_rotation = LocalRotation(initial_center=(-0.5, 0.25), scale=0.1)
displacements = v_rotation(torch.tensor(xy.reshape(-1, 2))).detach().numpy().reshape(num_x, num_y, -1)
fig1 = vis_grid_displacements(-displacements, xv=xv, yv=yv)
fig1.show()
# fig2 = vis_arrows(displacements, xv=xv, yv=yv)
# fig2.show()
# transf_rot = transformation(xy, v_rotation)
# transf_rot_2 = transformation(transf_rot, v_rotation)
# transf_rot_3 = transformation(transf_rot_2, v_rotation)
#
# fig4 = vis_grid(transf_rot_2)
# fig4.show()
#
# fig5 = vis_grid(transf_rot_3)
# fig5.show()

v_anis_scaling = LocalAnisotropicScaling(scale=0.5, initial_center=(0, 0), initial_alphas=(4, 2))
displacements = v_anis_scaling(torch.tensor(xy.reshape(-1, 2))).detach().numpy().reshape(num_x, num_y, -1)
fig6 = vis_grid_displacements(np.zeros_like(xy), xv=xv, yv=yv, color='gray')
fig6 = vis_grid_displacements(displacements, xv=xv, yv=yv, fig=fig6)
fig6.show()
fig7 = vis_arrows(displacements, xv=xv, yv=yv)
fig7.show()
