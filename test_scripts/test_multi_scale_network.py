import torch
import seaborn as sns
sns.set_color_codes('deep')

from local_diffeomorphism_networks import MultiScaleNetwork
import numpy as np

from plotting_utils import vis_grid, vis_grid_displacements

config_dict_1 = [{'type': 'scaling'}, {'type': 'rotation'}, {'type': 'translation'}] * 2
config_dict_2 = [{'type': 'scaling'}, {'type': 'rotation'}, {'type': 'translation'}] * 4


scales = [1, 0.5]

test_cls = MultiScaleNetwork(1, config_dicts=[config_dict_1, config_dict_2], scales=scales)
x = np.linspace(-1, 1, 10)
xv, yv = np.meshgrid(x, x, indexing='ij')
xy = np.stack([xv, yv], axis=2)
xy_flat = xy.reshape(-1, 2)

out = test_cls(torch.tensor(xy_flat, dtype=torch.float)).detach().numpy().reshape(xy.shape)
test_cls.velocity_fields[0].cost()
fig = vis_grid(out.reshape(xy.shape), xv=xv, yv=yv, zorder=1)
fig = vis_grid_displacements(np.zeros_like(xy), fig=fig, color='grey', alpha=0.3, zorder=0)
alphas = test_cls.velocity_fields[0].linear.weight.detach().cpu().numpy().squeeze()
# test_cls.velocity_fields[0].local_velocity_fields[2].plot(fig, s=alphas[2])
# test_cls.velocity_fields[0].local_velocity_fields[0].plot(fig, s=alphas[0])
# test_cls.velocity_fields[0].local_velocity_fields[1].plot(fig, s=alphas[1])
# test_cls.velocity_fields[0].local_velocity_fields[3].plot(fig, s=alphas[3])
# test_cls.velocity_fields[0].local_velocity_fields[4].plot(fig, s=alphas[4])
test_cls.velocity_fields[0].local_velocity_fields[5].plot(fig, s=alphas[5])

fig2 = vis_grid(out.reshape(xy.shape), xv=xv, yv=yv)
fig2 = vis_grid_displacements(np.zeros_like(xy), fig=fig2, color='grey', alpha=0.3)
fig2 = test_cls.velocity_fields[0].plot(fig=fig2, color='r')

fig2.show()
fig.show()

# fig3 = vis_grid(out.reshape(xy.shape), xv=xv, yv=yv)
# fig3 = vis_grid_displacements(np.zeros_like(xy), fig=fig3, color='grey', alpha=0.3)
# fig3 = test_cls.plot(fig3)
print(test_cls)
