from itertools import cycle

import torch

import torch.nn.functional as F
import numpy as np
from torch import optim

from skimage import io
import torchvision.transforms as transforms
from torch.utils.tensorboard import SummaryWriter

from local_diffeomorphism_networks import MultiScaleNetwork
from training_utils import plot_module, plot_true_and_transformed

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

# config_dict_1 = [{'type': 'scaling'}, {'type': 'rotation', 'params': {'initial_center': [0.1, -0.1]}}, {'type': 'translation'}]
config_dict_1 = [{'type': 'anisotropic_scaling', 'params': {'initial_center': [-0.4, 0.6], 'initial_alphas': [1, 0.5]}
                  }]
config_dict_2 = [{'type': 'rotation', 'params': {'initial_center': [0.1, -0.1]}
                  }]
# config_dict_2 = [{'type': 'translation', 'params': {'initial_center': [-0.2, -0.8], 'initial_direction': [1, 1]}}]
# config_dict_1 = [{'type': 'translation'}] * 5

scales = [5]
train_network = MultiScaleNetwork(1, config_dicts=[config_dict_1], scales=scales)
train_network.to(device)

# define input data
x = np.linspace(-1, 1, 200)
xv, yv = np.meshgrid(x, x, indexing='ij')
xy = np.stack([xv, yv], axis=2)
xy_flat = xy.reshape(-1, 2)
xy_flat_tensor = torch.tensor(xy_flat, dtype=torch.float)

# train_network.initialize_random_centers_(xy_flat)
train_network.velocity_fields[0].set_alphas_([1])

# optimizer1 = optim.Adam(train_network.velocity_fields[0].parameters(), lr=0.01)
# optimizer2 = optim.Adam(train_network.velocity_fields[1].parameters(), lr=0.01)
# optimizers = cycle([optimizer1, optimizer2])

optimizer = optim.Adam(train_network.parameters(), lr=0.01)


input_image = io.imread('square_data/square1_norot.png', as_gray=True)
target_image = io.imread('square_data/square2_norot.png', as_gray=True)

transform = transforms.Compose([transforms.ToPILImage(), transforms.Resize(200), transforms.ToTensor()])

delta = 0.01

loss_mean = torch.nn.MSELoss(reduction='mean')

save_path = 'runs/square/norot/test3'
writer = SummaryWriter(save_path)

for epoch in range(1000):

    def calculate_losses(outputs):
        deformed_image = F.grid_sample(transform(input_image).transpose(0, 1).unsqueeze_(dim=0),
                                       grid=outputs.view(1, xy.shape[0], xy.shape[1], 2).transpose(1, 2)[..., [1, 0]],
                                       padding_mode='border', align_corners=False).transpose(1, 2)

        loss1 = loss_mean(transform(target_image).squeeze(), deformed_image.squeeze())
        loss_cost = train_network.cost()
        loss_total = 1 / delta * loss1 + loss_cost
        # loss1 += train_network.velocity_fields[0].linear.weight.item()**2
        return loss_total, loss1, loss_cost, deformed_image


    def closure():
        # zero the parameter gradients
        optimizer.zero_grad()
        outputs = train_network(xy_flat_tensor)
        loss_total = calculate_losses(outputs)[0]
        loss_total.backward()
        return loss_total


    #optimizer.step(closure)

    with torch.no_grad():
        outputs = train_network(xy_flat_tensor)
        loss_total, loss1, loss_cost, deformed_image = calculate_losses(outputs)
        # print statistics
        if epoch % 100 == 0:
            writer.add_scalar('loss', loss_total.item(), epoch)
            writer.add_figure('deformation', plot_module(train_network, xv, yv, subsample=5), global_step=epoch)
            writer.add_figure('output_vs_target',
                              plot_true_and_transformed(train_network, transform(input_image), deformed_image, transform(target_image), xv, yv),
                              global_step=epoch)
            print(f'[{epoch + 1:d}] loss: {loss1.item():.3f}     '
                  f'cost: {loss_cost.item():.3f}    '
                  f'center: {train_network.velocity_fields[0].local_velocity_fields[0].center.data}    '
                  f'alpha (scalar): {train_network.velocity_fields[0].linear.weight.data}   '
                  f'alphas: {train_network.velocity_fields[0].local_velocity_fields[0].alphas.data}')

writer.close()
torch.save(train_network.state_dict(), save_path + 'state_dict.pth')
