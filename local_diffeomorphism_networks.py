from collections import defaultdict

import torch
from velocity_fields_pytorch import LocalRotation, LocalScaling, LocalTranslation, LocalAnisotropicScaling
import numpy as np

# dictionary to correlate config names to classes
classes_dict = {'scaling': LocalScaling,
                'rotation': LocalRotation,
                'translation': LocalTranslation,
                'anisotropic_scaling': LocalAnisotropicScaling}


class SumOfVelocityFields(torch.nn.Module):
    """
    Module which calculates the sum of local velocity fields on the same scale, which are specified
    in a config dict.
    """

    def __init__(self, config_dict_list, scale):
        """

        :param config_dict_list: list of dictionary to specify which velocity fields of which type the module should use
                    of the following form:
                    each dictionary should have the following form:
                    {'type': TYPE, 'params': {PARAM1: VALUE1, ...}, 'alpha': VALUE}
                    where TYPE is one of 'scaling', 'rotation', 'translation' and params are initialization parameters
                    for the corresponding class

        """
        super().__init__()
        velocity_fields = []
        alphas = []
        for config_dict in config_dict_list:
            params = config_dict.get('params', {})
            alpha = config_dict.get('alpha', np.NaN)
            type_name = config_dict['type']
            velocity_field = classes_dict[type_name](scale=scale, **params)
            velocity_fields.append(velocity_field)
            alphas.append(alpha)
        self.local_velocity_fields = torch.nn.ModuleList(velocity_fields)
        self.scale = scale
        self.config_dict = config_dict_list
        self.linear = torch.nn.Linear(in_features=len(velocity_fields), out_features=1, bias=False)
        # replace nan values in alphas by standard initialization
        alphas_before = self.linear.weight.detach().numpy()
        alphas_after = np.where(np.isnan(np.asarray(alphas)), alphas_before, alphas)
        self.set_alphas_(alphas_after)

    def forward(self, xy: torch.Tensor) -> torch.Tensor:
        """
        Returns :math:`\sum_{i=1}^N α_i v_i(x,y)` where N is the number of local velocity fields.

        :param xy: shape (batch_size, 2)
        :return: result shape (batch_size, 2)
        """
        velocity_vectors = []
        for velocity_field in self.local_velocity_fields:
            velocity_vectors.append(velocity_field(xy))
        # stack velocity vectors in order to put them into linear layer,
        # resulting shape is (batch_size, 2, num_velocity_fields)
        velocity_vectors = torch.stack(velocity_vectors, dim=-1)
        velocity_vector = self.linear(velocity_vectors).squeeze()
        return velocity_vector

    def reset_alphas_(self):
        """
        Reset linear parameters to default PyTorch initialization.
        """
        self.linear.reset_parameters()

    def set_centers_(self, centers):
        """
        Set centers of local velocity fields (order as in self.local_velocity_fields).
        :param centers: centers
        """
        for velocity_field, center in zip(self.local_velocity_fields, centers):
            velocity_field.set_center_(center)

    def initialize_random_centers_(self, xy):
        """
        Initialize centers of local velocity fields with random distinct elements from the given coordinates in xy.
        :param xy: list of coordinates to choose from
        """
        num_centers = len(self.local_velocity_fields)
        # get num_centers distinct coordinates from xy
        random_centers = xy[np.random.choice(len(xy), num_centers, replace=False)]
        self.set_centers_(random_centers)

    def set_alphas_(self, alphas):
        """
        Initialize linear parameters with alphas.
        :param alphas: list or numpy array of values
        """
        # see https://discuss.pytorch.org/t/what-the-recommended-way-to-update-tensors-value/21766
        with torch.no_grad():
            self.linear.weight.copy_(torch.tensor(alphas, dtype=torch.float))

    def plot(self, fig=None, color=None):
        if fig is None:
            import matplotlib.pyplot as plt
            fig = plt.figure()
        alphas = self.linear.weight.detach().cpu().numpy().reshape(-1)
        for velocity_field, alpha in zip(self.local_velocity_fields, alphas):
            velocity_field.plot(fig, s=alpha, color=color)
        return fig

    def regularization_loss(self, p):
        return torch.norm(self.linear.weight, p=p)

    def redundancy_loss(self, lmbd=1):
        redundancy_loss_dict = defaultdict(list)
        for velocity_field in self.local_velocity_fields:
            redundancy_loss_dict[velocity_field.__class__.__name__].append(velocity_field.center)
        redundancy_losses = []
        for centers in redundancy_loss_dict.values():
            if len(centers) > 1:
                centroid = torch.sum(torch.stack(centers), dim=0)/len(centers)
                for center in centers:
                    redundancy_losses.append(torch.norm(center-centroid))
        redundancy_loss = torch.sum(torch.stack(redundancy_losses))
        return lmbd * 1./redundancy_loss

    def disturb_(self, sigma_alphas, sigma_centers):
        """
        Add zero-mean gaussian noise to alphas and centers, with specified standard deviation.
        :param sigma_alphas: standard deviation for noise added to alphas
        :param sigma_centers: standard deviation for noise added to centers
        """
        noise_alphas = np.random.normal(scale=sigma_alphas, size=self.linear.weight.shape)
        old_alphas = self.linear.weight.detach().cpu().numpy()
        self.set_alphas_(old_alphas + noise_alphas)
        for velocity_field in self.local_velocity_fields:
            velocity_field.disturb_(sigma_centers, )  # TODO

    def cost(self):
        translation_centers = []
        translation_alphas = []
        translations = []
        costs = []
        for velocity_field, alpha in zip(self.local_velocity_fields, self.linear.weight.data.view(-1)):
            if velocity_field.__class__ == classes_dict['translation']:
                translation_alphas.append(alpha)
                translation_centers.append(velocity_field.center)
                translations.append(velocity_field)
            else:
                if velocity_field.__class__ != classes_dict['scaling']:
                    # TODO
                    costs.append(velocity_field.cost(alpha))
        if len(translations) > 0:
            translation_vectors = [translation_alphas[i] * translations[i].direction for i in range(len(translations))]
            costs.append(translations[0].abstract_cost(translation_centers, translation_vectors))
        return torch.sum(torch.stack(costs))


class MultiScaleNetwork(torch.nn.Module):

    def __init__(self, tau, config_dicts=None, scales=None, velocity_fields=None):
        """
        Initialize using either config_dicts and scales, which them uses SumOfVelocityFields, or list of already
        initialized velocity fields (inheriting from ParametrizedLocalVelocityField)
        :param tau: step size
        :param config_dicts: list of config dicts as used in SumOfVelocityFields
        :param scales: list of scales
        :param velocity_fields: list of initialized velocity fields
        """
        super().__init__()
        self.tau = tau
        if config_dicts is None or scales is None:
            if velocity_fields is None:
                raise ValueError('Must provide either velocity_fields or both config_dicts and scales.')
        else:
            if velocity_fields is not None:
                raise ValueError('Only provide one of velocity_fields or config_dicts and scales.')
            self.config_dicts = config_dicts
            self.scales = scales
            velocity_fields = [SumOfVelocityFields(config_dict, scale) for config_dict, scale in
                               zip(config_dicts, scales)]
        self.velocity_fields = torch.nn.ModuleList(velocity_fields)

    def forward(self, xy):
        """
        Calculates the composition of transforms resulting from velocity fields. A transform is given by

        .. math:: \phi_\sigma(x,y) = (x,y) + 𝜏 v_\sigma(x,y)

        :param xy: shape (batch_size, 2)
        :return: result shape (batch_size, 2)
        """
        out = xy
        for velocity_field in self.velocity_fields:
            velocity_vector = velocity_field(out)
            out = out + self.tau * velocity_vector
        return out

    def calculate_all_scales(self, xy):
        """
        Return not only the final scale, but results on all scales.
        :param xy: shape (batch_size, 2)
        :return: result list of tensors of shape (batch_size, 2) of length len(self.velocity_fields)
        """
        outs = []
        out = xy
        for velocity_field in self.velocity_fields:
            velocity_vector = velocity_field(out)
            out = out + self.tau * velocity_vector
            outs.append(out)
        return outs

    def initialize_random_centers_(self, xy):
        """
        Initialize velocity field on each scale with random centers, where the centers in each velocity field are
        disjoint, but the same centers can appear on different scales.
        :param xy: list of coordinates to choose from
        """
        for velocity_field in self.velocity_fields:
            velocity_field.initialize_random_centers_(xy)

    def specify_trainable_scales_(self, scale_indices):
        trainables = 0
        scale_indices = set(scale_indices)
        for i, module in enumerate(self.velocity_fields):
            if i in scale_indices:
                module.set_trainable_(True)
                trainables += 1
        assert len(scale_indices), 'The number of trainable layers does not correspond ' \
                                   'to the number of scales in scale_indices. Check scale_indices.'

    def get_alphas(self):
        return [{'scale': scale, 'alphas': velocity_field.linear.weight.detach().numpy()} for velocity_field, scale in
            zip(self.velocity_fields, self.scales)]

    def plot(self, fig=None, color_palette=None):
        if color_palette is None:
            import seaborn as sns
            color_palette = sns.color_palette('deep')
        legend_elements = []
        from matplotlib.lines import Line2D
        for i, velocity_field in enumerate(self.velocity_fields):
            velocity_field.plot(fig=fig, color=color_palette[i])
            legend_elements.append(Line2D([0], [0], marker='o', color='w', label=f'Scale: {self.scales[i]}',
                                          markerfacecolor=color_palette[i], markersize=15))
            fig.gca().legend(handles=legend_elements)
        return fig

    def regularization_loss(self, p, lmbd=None):
        if lmbd is None:
            lmbd = 1
        if np.isscalar(lmbd):
            lmbd = [lmbd] * len(self.scales)
        lp_regs = []
        for velocity_field, lmbd in zip(self.velocity_fields, lmbd):
            lp_regs.append(lmbd * velocity_field.regularization_loss(p))
        return torch.sum(torch.stack(lp_regs))

    def redundancy_loss(self, lmbd=None):
        if lmbd is None:
            lmbd = 1
        if np.isscalar(lmbd):
            lmbd = [lmbd] * len(self.scales)
        redundancy_losses = []
        for velocity_field, lmbd in zip(self.velocity_fields, lmbd):
            redundancy_losses.append(lmbd * velocity_field.redundancy_loss())
        return torch.sum(torch.stack(redundancy_losses))

    def get_parameter_device(self):
        return next(self.parameters()).device

    def disturb_(self, sigma_alphas, sigma_centers):
        """
        Add gaussian noise to model parameters: zero-mean gaussian noise with standard deviation sigma_alphas*scale[i]
        for alphas, zero-mean gaussian noise with standard deviation scale_centers*scale[i] for centers,
        where 0 <= i <= number of scales in MultiScaleNetwork
        :param sigma_alphas: standard deviation for alphas
        :param sigma_centers: standard deviation for centers
        """
        for i, velocity_field in enumerate(self.velocity_fields):
            velocity_field.disturb_(sigma_alphas * self.scales[i], sigma_centers * self.scales[i])

    def cost(self):
        costs = [velocity_field.cost() for velocity_field in self.velocity_fields]
        return torch.sum(torch.stack(costs))
