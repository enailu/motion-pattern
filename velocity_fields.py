import numpy as np


def transformation(xy, velocity_func, step=1):
    """
    Calculate transformation f(x,y) = (x,y) + step*v(x,y).
    :param xy: input x, y as numpy array of shape (num_x, num_y, 2), e.g. obtained from meshgrid xv, yv via stack along
    last dimension
    :param velocity_func: function taking xv, yv as input and returning vectors of shape (num_x, num_y, 2)
    :param step: step
    :return: transformation applied to xy, numpy array of shape (num_x, num_y, 2)
    """
    return xy + step*velocity_func(xy[..., 0], xy[..., 1])


def gaussian_kernel_func(center, sigma=1):
    """
    Gaussian kernel function, K_sigma(·, center) = exp(-(·-center)²/(2sigma²)
    :param center: list or tuple of two coordinates (x,y)
    :param sigma: sigma (scaling factor)
    :return: function taking xv, yv from meshgrid as input
    """

    def apply(xv, yv):
        return np.expand_dims(np.exp(-((xv - center[0]) ** 2 + (yv - center[1]) ** 2) / (2 * sigma ** 2)), -1)

    return apply


def translation(direction, center, sigma=1):
    """
    Vector field representing sum of translations, v(·)=sum_i^n controls[i] * K_sigma(·, centers[i]) * directions[i]
    :param direction: tuple or array with two elements, x and y coordinate
    :param center: tuple or array with two elements, x and y coordinate
    :param sigma: sigma (scaling factor)
    :return: function taking xv, yv from meshgrid as input and returning numpy array of shape (num_x, num_y, 2)
    """

    def apply(xv, yv):
        result = gaussian_kernel_func(center, sigma)(xv, yv) * direction
        return result

    return apply


def sum_of_translations(directions, controls, centers, sigma=1):
    """
    Vector field representing sum of translations, v(·)=sum_i^n controls[i] * K_sigma(·, centers[i]) * directions[i]
    :param directions: list or numpy array of n 2D vectors, either one direction or n directions
    :param controls: list or numpy array of n scalars
    :param centers: list or numpy array of n 2D vectors
    :param sigma: sigma (scaling factor)
    :return: function taking xv, yv from meshgrid as input and returning numpy array of shape (num_x, num_y, 2)
    """
    directions = np.asarray(directions)
    controls = np.asarray(controls)
    centers = np.asarray(centers)
    if len(directions) == 1:
        directions = np.broadcast_to(directions, centers.shape)
    assert len(directions) == len(controls) == len(centers)

    directions = np.reshape(directions, (-1, 1, 1, 2))

    def apply(xv, yv):
        return np.sum([control * translation(direction, center, sigma)(xv, yv) for control, direction, center in
                       zip(controls, directions, centers)], axis=0)

    return apply


def rotation(center, sigma=1):
    """
    Velocity field representing sum of local rotations.
    :param center: tuple or array with two elements, x and y coordinate
    :param sigma: sigma (locality factor)
    :return: function taking xv, yv from meshgrid as input and returning numpy array of shape (num_x, num_y, 2)
    """
    sqrt3 = np.sqrt(3)
    d1 = np.array((0.5, 0.5 * sqrt3))
    d2 = np.array((0.5, -0.5 * sqrt3))
    d3 = np.array((-1, 0))
    z1 = sigma / 3 * np.array((0.5 * sqrt3, -0.5))
    z2 = sigma / 3 * np.array((-0.5 * sqrt3, -0.5))
    z3 = sigma / 3 * np.array((0, 1))

    def apply(xv, yv):
        result = gaussian_kernel_func(center + z1, sigma)(xv, yv) * d1 \
                  + gaussian_kernel_func(center + z2, sigma)(xv, yv) * d2 \
                  + gaussian_kernel_func(center + z3, sigma)(xv, yv) * d3
        return result

    return apply


def sum_of_rotations(centers, controls, sigma=1):
    """
    Velocity field representing sum of local rotations.
    :param centers: rotation centers, list or numpy array of n 2D vectors
    :param controls: list or numpy array of n scalars
    :param sigma: sigma (locality factor)
    :return: function taking xv, yv from meshgrid as input and returning numpy array of shape (num_x, num_y, 2)
    """
    centers = np.asarray(centers)
    controls = np.asarray(controls)
    assert len(centers) == len(controls)

    def apply(xv, yv):
        return np.sum([control * rotation(center, sigma)(xv, yv) for control, center in zip(controls, centers)], axis=0)

    return apply


def scaling(center, sigma):
    """
    Velocity field representing local scaling at center on scale sigma.
    :param center: tuple or array with two elements, x and y coordinate
    :param sigma: sigma
    :return: function taking xv, yv from meshgrid as input and returning numpy array of shape (num_x, num_y, 2)
    """
    sqrt3 = np.sqrt(3)
    # make these into a constant in a class
    d1 = np.array((0.5 * sqrt3, -0.5))
    d2 = np.array((-0.5 * sqrt3, -0.5))
    d3 = np.array((0, 1))

    def apply(xv, yv):
        result = gaussian_kernel_func(center + sigma / 3 * d1, sigma)(xv, yv) * d1 \
                + gaussian_kernel_func(center + sigma / 3 * d2, sigma)(xv, yv) * d2 \
                + gaussian_kernel_func(center + sigma / 3 * d3, sigma)(xv, yv) * d3
        return result

    return apply


def sum_of_scalings(centers, controls, sigma):
    """
    Velocity field representing sum of local scalings. No scaling is obtained for control → 0.
    :param centers: scaling centers, list or numpy array of n 2D vectors
    :param controls: list or numpy array of n scalars
    :param sigma: sigma (locality factor)
    :return: function taking xv, yv from meshgrid as input and returning numpy array of shape (num_x, num_y, 2)
    """
    centers = np.asarray(centers)
    controls = np.asarray(controls)
    assert len(centers) == len(controls)

    def apply(xv, yv):
        return np.sum([control * scaling(center, sigma)(xv, yv) for control, center in zip(controls, centers)], axis=0)
    return apply
